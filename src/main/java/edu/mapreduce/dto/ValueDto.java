package edu.mapreduce.dto;

public class ValueDto {
    // Количество запросов
    public int requestCount;
    // Суммарная длина запросов
    public int sumLength;
    // средняя длина запросов
    public double avgLength;

    public ValueDto(int requestCount, int sumLength, double avgLength) {
        this.requestCount = requestCount;
        this.sumLength = sumLength;
        this.avgLength = avgLength;
    }
}

