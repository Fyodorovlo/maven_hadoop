package edu.mapreduce;

import edu.mapreduce.counters.InvalidIpRowsCounter;
import edu.mapreduce.counters.InvalidSeparatorRowsCounter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import org.apache.hadoop.io.compress.SnappyCodec;

import org.apache.hadoop.mapreduce.Counter;

import org.apache.hadoop.io.*;

/**
 * Главный запускаемый класс
 */
public class IpLogsMapReduce {

    public static void main(String[] args) throws Exception {
        // Создание конфигурации приложения
        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator", ",");
        // настройка сжатия при помощи SnappyCodec
        conf.set("mapreduce.map.output.compress", "true");
        conf.set("mapreduce.map.output.compress.codec", "org.apache.hadoop.io.compress.SnappyCodec");
        // Настройка map reduce job
        Job job = Job.getInstance(conf, "max word");
        // Задание main класса
        job.setJarByClass(IpLogsMapReduce.class);
        // Задание класса для mapper
        job.setMapperClass(IpMapper.class);
        // Задание класса для reducer
        job.setReducerClass(IpReducer.class);
        // Задание класса для combiner
        job.setCombinerClass(IpReducer.class);
        // Настройка вывода
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        // Использование SequenceFile для вывода
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        // настройка сжатия при помощи SnappyCodec
        FileOutputFormat.setCompressOutput(job, true);
        FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);
        // Задание входного и выходного файла
        FileInputFormat.addInputPath(job, new Path(args[1]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        //Запуск и логирование времени
        long startTime = System.nanoTime();
        boolean success = job.waitForCompletion(true);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        if (!success) {
            // Ошибка на этапе исполнения
            System.out.println("Job exited with error!!!");
        } else {
            // Исполнение прошло успешно
            System.out.println("Success!!! Duration: " + (duration / 1000000) + "ms");
            // Получить занчения счетчиков
            Counter invalidIpCounter = job.getCounters().findCounter(InvalidIpRowsCounter.INVALID_IP);
            Counter invalidSeparatorCounter = job.getCounters().findCounter(InvalidSeparatorRowsCounter.INVALID_SEPARATOR);
            // Залогировать значения счетчиков
            System.out.println("INVALID_IP=[" + invalidIpCounter.getValue() + "]");
            System.out.println("INVALID_SEPARATOR=[" + invalidSeparatorCounter.getValue() + "]");
        }
    }
}
