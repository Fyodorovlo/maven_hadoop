package edu.mapreduce;

import com.google.gson.Gson;
import edu.mapreduce.dto.ValueDto;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * агрегирует пары вида <Text,Text>
 * 0.0.0.0 -  {"requestCount":1,"sumLength":134,"avgLength":134.0}
 * где
 *      requestCount количество запросов на данный ip
 *      sumLength - суммарная длина всех запросов на данный ip
 *      avgLength - средняя длина запросов на данный ip
 *
 * Reduces суммируется requestCount и sumLength пар и вычисляет avgLength.
 */
public class IpReducer extends Reducer<Text, Text, Text, Text> {

    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Gson gson = new Gson();
        // Считается суммарное количество и суммарная длина запросов
        int sumLength = 0;
        int requestCount = 0;
        for (Text val : values) {
            ValueDto valueDto = gson.fromJson(val.toString(), ValueDto.class);
            requestCount += valueDto.requestCount;
            sumLength += valueDto.sumLength;
        }
        // Вычисления записываются в json. avgLength рассчитывается как (double) sumLength / requestCount
        ValueDto valueDto = new ValueDto(requestCount, sumLength, (double) sumLength / requestCount);
        String json = gson.toJson(valueDto);
        //результат передается дальше
        context.write(key, new Text(json));
    }
}