#/usr/bin/env bash

echo 'INFO: docker  build -t maven/hadoop:2.9.0 ./image/'
docker build -t maven/hadoop:2.9.0 ./image/

echo 'INFO: docker-compose  -f cluster.yml up -d'
docker-compose -f cluster.yml up -d

echo 'INFO: remove input/output HDFS directories if they already exist'
docker exec namenode runuser -l hadoop -c $'hdfs dfs -rm -R input'
docker exec namenode runuser -l hadoop -c $'hdfs dfs -rm -R output'

echo 'INFO: hdfs dfs -mkdir -p /user/hadoop/input'
docker exec namenode runuser -l hadoop -c $'hdfs dfs -mkdir -p /user/hadoop/input'

echo 'INFO: hdfs dfs -put /user/hadoop/file /user/hadoop/input/'
docker exec namenode runuser -l hadoop -c $'hdfs dfs -put /user/hadoop/file /user/hadoop/input/'

echo 'INFO: hdfs dfs -put /user/hadoop/file2 /user/hadoop/input/'
docker exec namenode runuser -l hadoop -c $'hdfs dfs -put /user/hadoop/file2 /user/hadoop/input/'

echo 'INFO: hadoop jar /IpLogsMapReduce.jar IpLogsMapReduce input output'
docker exec namenode runuser -l hadoop -c $'hadoop jar /IpLogsMapReduce.jar IpLogsMapReduce input output'

echo 'INFO: hdfs dfs -ls /user/hadoop/output'
docker exec namenode runuser -l hadoop -c $'hdfs dfs -ls /user/hadoop/output'

#echo 'INFO: hdfs dfs -text /user/hadoop/output/part-r-00000'
#docker exec namenode runuser -l hadoop -c $'hdfs dfs -text /user/hadoop/output/part-r-00000'

echo 'HDFS directories at: http://localhost:50070/explorer.html#/user/hadoop'

exit 0;
